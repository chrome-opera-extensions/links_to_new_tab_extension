var cmenu_clicked = [], tabId;

function getTabId() {
	var _tabId;
	chrome.tabs.query({
		"active": true,
		"lastFocusedWindow": true
	}, (tabs) => { 
		_tabId = tabs[0].id
	});
	return _tabId;
}

function setBlanks(evt) {
	chrome.tabs.query({
		"active": true,
		"lastFocusedWindow": true
	}, (tabs) => {
		chrome.tabs.executeScript(tabs[0].id, {
			code: `[...document.links].forEach((a) => { a.target = "_blank"; a.dataset.ctextmenu = true; });`
		}, (results) => {
			if(results) { 
				console.log("Links updated on tab id:", tabs[0].id);
				if(!cmenu_clicked.includes(tabs[0].id)) {
					cmenu_clicked.push(tabs[0].id);
					tabId = tabs[0].id;
					createContextMenu();
				}
			}
		});
	});
}

var cmenu = null;
function createContextMenu() {
	var menuText = (cmenu_clicked.includes(tabId)) ? "✔ Links to New Tab" : "Links to New Tab";
	var context = "page";
	var optionsNew = {
		title: menuText,
		//type: "checkbox",
		contexts: [context],
		id: "context" + context,
		onclick: setBlanks
	}
	var optionsUpdate = {
		title: menuText,
		//type: "checkbox",
		contexts: [context],
		onclick: setBlanks
	}
	//console.log("cmenu:", cmenu);
	if(cmenu) {
		chrome.contextMenus.update(cmenu, optionsUpdate);
	} else {
		cmenu = chrome.contextMenus.create(optionsNew);
	}
}
//var useBlanks = chrome.contextMenus.create({"title": "Links to New Tab", "type": "checkbox", "onclick":setBlanks});
//console.log("useBlanks:", useBlanks);

chrome.runtime.onInstalled.addListener((details) => {
	createContextMenu();
});

chrome.tabs.onCreated.addListener((e) => {
	if(chrome.extension.inIncognitoContext) {
		createContextMenu();
	}
	if(!chrome.extension.inIncognitoContext) {
		createContextMenu();
	}
});

chrome.tabs.onActivated.addListener(function(activeInfo) {
  // how to fetch tab url using activeInfo.tabid
  //chrome.tabs.get(activeInfo.tabId, function(tab){
  //   console.log(tab.url);
  //});
  tabId = activeInfo.tabId;
  //console.log("activeInfo.tabId:", activeInfo.tabId);
  createContextMenu();
}); 